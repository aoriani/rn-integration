package com.reaja;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import com.react.ReactApp;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        layout.setGravity(Gravity.CENTER);
        Button button = new Button(this);
        button.setText("Launch React app");
        button.setOnClickListener(new OnClickListener() {
            public void onClick (View v) {
                Intent intent = new Intent(MainActivity.this, ReactApp.class);
                startActivity(intent);
            }
        });
        button.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        layout.addView(button);
        setContentView(layout);
    }
}