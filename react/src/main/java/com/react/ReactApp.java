package com.react;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Collections;
import java.util.List;

public class ReactApp extends ReactActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getMainComponentName() {
    	return "ReactApp"; 
    }

    @Override
    protected boolean getUseDeveloperSupport() {
    	return false;
    }

    @Override
    protected String getJSMainModuleName() {
    	return "react_app.android";
  	}

  	@Override
  	protected String getBundleAssetName() {
    return "react_app.android.bundle";
  	};

    @Override
    protected List<ReactPackage> getPackages() {
    	return Collections.<ReactPackage>singletonList(new MainReactPackage());
    }

}